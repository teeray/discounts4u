package io.swagger.api;

import za.co.taylor.dto.Customer;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-08-19T08:42:43.883Z")

@Api(value = "register", description = "the register API")
public interface RegisterApi {

    @ApiOperation(value = "", notes = "adds a customer to the database with relevant information", response = Void.class, tags={ "registration", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "registration successful", response = Void.class),
        @ApiResponse(code = 400, message = "invalid/incomplete input", response = Void.class) })
    @RequestMapping(value = "/register",
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> registerCustomer(@ApiParam(value = "the customer object to add"  ) @RequestBody Customer customer);

}
