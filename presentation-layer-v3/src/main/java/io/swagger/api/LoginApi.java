package io.swagger.api;


import io.swagger.annotations.*;
import za.co.taylor.dto.Customer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-08-19T08:42:43.883Z")

@Api(value = "login", description = "the login API")
public interface LoginApi {

    @ApiOperation(value = "", notes = "customer or admin enters username and password to login", response = Void.class, tags={ "login", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "login successful", response = Void.class),
        @ApiResponse(code = 400, message = "invalid credentials", response = Void.class) })
    @RequestMapping(value = "/login",
        method = RequestMethod.POST)
    ResponseEntity<Customer> login(@ApiParam(value = "the username to login with", required = true) @RequestParam(value = "username", required = true) String username,
                                                    @ApiParam(value = "the password to login with", required = true) @RequestParam(value = "password", required = true) String password);

}
