package io.swagger.api;


import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import za.co.taylor.dto.Customer;
import za.co.taylor.dto.DTOBuilder;
import za.co.taylor.dto.DTOBuilderImpl;
import za.co.taylor.entity.CustomerEntity;
import za.co.taylor.services.LoginService;

import java.util.List;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-08-19T08:42:43.883Z")

@Controller
public class LoginApiController implements LoginApi {

    @Autowired
    LoginService service;

    @Autowired
    DTOBuilder dtoBuilder;

    public ResponseEntity<Customer> login(@ApiParam(value = "the username to login with", required = true) @RequestParam(value = "username", required = true) String username,
        @ApiParam(value = "the password to login with", required = true) @RequestParam(value = "password", required = true) String password) {
        CustomerEntity customerEntity = this.service.login(username, password);

        Customer customer = dtoBuilder.entityToCustomerDTO(customerEntity);

        return new ResponseEntity<Customer>(customer,HttpStatus.OK);
    }

}
