package io.swagger.api;

import org.springframework.beans.factory.annotation.Autowired;
import za.co.taylor.dto.Customer;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import za.co.taylor.entity.CustomerEntity;
import za.co.taylor.entity.EntityBuilder;
import za.co.taylor.services.RegistrationService;

import java.util.List;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-08-19T08:42:43.883Z")

@Controller
public class RegisterApiController implements RegisterApi {

    @Autowired
    RegistrationService registrationService;

    public ResponseEntity<Void> registerCustomer(@ApiParam(value = "the customer object to add"  ) @RequestBody Customer customer) {
        CustomerEntity ce = new EntityBuilder().dtoToCustomerEntity(customer);

        registrationService.register(ce);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
