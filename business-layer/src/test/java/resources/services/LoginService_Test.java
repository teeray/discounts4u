package resources.services;

import resources.dao.DAO_Test;
import za.co.taylor.entity.CustomerEntity;
import za.co.taylor.services.LoginService;

public class LoginService_Test implements LoginService {

    DAO_Test<CustomerEntity> dao;

    public void setDao(DAO_Test daoInterfaceTest){
        this.dao = daoInterfaceTest;
    }

    @Override
    public CustomerEntity login(String username, String password) {

        CustomerEntity customer = (CustomerEntity)dao.rowExists(username, password);

        if ( customer != null){
            return customer;
        } else return null;
    }
}
