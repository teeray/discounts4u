package resources.services;

import resources.dao.DAO_Test;
import za.co.taylor.entity.DiscountEntity;

import java.util.List;

public class DiscountService_Test {

    DAO_Test<DiscountEntity> dao;

    public DiscountService_Test() {
        //this.resources.dao= new DAOImpl_Test<>(DiscountEntity.class);
    }

    public void setdaoInterfaceTest(DAO_Test daoInterfaceTest){
        this.dao = daoInterfaceTest;
    }

    public List<DiscountEntity> getDiscounts(){
        return dao.getAll();
    }

    public String deleteDiscount(String id){
        return dao.delete(id);
    }

    public String addDiscount(DiscountEntity de){
        return dao.create(de);
    }

    public DiscountEntity updateDiscount(DiscountEntity d){
        return dao.update(d);
    }
}
