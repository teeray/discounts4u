package resources.services;

import resources.dao.DAO_Test;
import za.co.taylor.dao.DAO;
import za.co.taylor.dao.DAOImpl;
import za.co.taylor.entity.CustomerEntity;
import za.co.taylor.services.RegistrationService;

public class RegistrationService_Test {

    DAO_Test<CustomerEntity> dao;

    public void setDAO(DAO_Test daoInterfaceTest){
        this.dao = daoInterfaceTest;
    }

    public String register(CustomerEntity ce) {
        return this.dao.create(ce);
    }
}
