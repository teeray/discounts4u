package resources.dao;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.WriteConcern;
import org.mongojack.DBCursor;
import org.mongojack.DBQuery;
import org.mongojack.JacksonDBCollection;
import za.co.taylor.entity.BaseEntity;
import za.co.taylor.entity.CustomerEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DAOImpl_Test<T extends BaseEntity> implements DAO_Test<T> {
    private JacksonDBCollection<T,String> wrappedCollection;

    public DAOImpl_Test(){

    }

    public DAOImpl_Test(Class<T> tClass, String collName){
        Mongo client = new Mongo("localhost", 27017);
        client.setWriteConcern(WriteConcern.ACKNOWLEDGED);
        DB db = client.getDB("mongotest");
        DBCollection coll = db.getCollection(collName);
        wrappedCollection = JacksonDBCollection.wrap(coll, tClass, String.class);
    }

    //Working with discount

    @Override
    public String create(T entity) {

        if(!rowExists(entity.get_id())) {
            entity.initData();
            return this.wrappedCollection.insert(entity).getSavedId();
        } else return "User already exists";
    }

    @Override
    public T update(T entity) {
        T oldEntity = wrappedCollection.findOneById(entity.get_id());

        entity.setDateCreated(oldEntity.getDateCreated());
        entity.setLastUpdate(new Date(System.currentTimeMillis()));
        this.wrappedCollection.save(entity);

        return entity;
    }

    @Override
    public String delete(String id) {
        //DBQuery.Query query = DBQuery.is("_id", id);
        this.wrappedCollection.removeById(id);
        return id;
    }

    @Override
    public List<T> getAll() {
        List<T> list = new ArrayList<>();
        DBCursor<T> cursor = wrappedCollection.find();

        while (cursor.hasNext()){
            list.add(cursor.next());
        }
        return list;
    }

    @Override
    public T getById(String id) {
        return this.wrappedCollection.findOneById(id);
    }

    @Override
    public T rowExists(String username, String password) {
        DBQuery.Query checkUsername = DBQuery.is("username", username);

        DBCursor<T> cursor = this.wrappedCollection.find(checkUsername);

        if(cursor.size() == 0){
            return null;
        } else {
            CustomerEntity customerToCheck = (CustomerEntity) cursor.next();

            if (customerToCheck.getPassword().equals(password)){
                return (T)customerToCheck;
            } else return null;
        }
    }

    public boolean rowExists(String id){
        T entity = this.wrappedCollection.findOneById(id);

        return entity == null ? false : true;
    }

//    @Override
//    public void getOne(T entity) {
//        System.out.println(wrappedCollection.findOneById(entity.get_id()));
//    }
//
//    //Working with customer
//    @Override
//    public String createCustomer(T entity) {
//        entity.initData();
//        return this.wrappedCollection.insert(entity).getSavedId();
//    }
//
//    @Override
//    public int updateCustomer(T entity) {
//        CustomerEntity ce = (CustomerEntity)this.wrappedCollection.findOneById(entity.get_id());
//
//        entity.setDateCreated(entity.getDateCreated());
//        entity.setLastUpdate(new Date(System.currentTimeMillis()));
//
//        return this.wrappedCollection.save(entity).getN();
//    }
//
//    @Override
//    public int deleteCustomer(String id) {
//        return this.wrappedCollection.removeById(id).getN();
//    }
//
//    @Override
//    public List<T> getAllCustomers() {
//        List<T> list = new ArrayList<>();
//        DBCursor<T> cursor = this.wrappedCollection.find();
//
//        while(cursor.hasNext()){
//            list.add(cursor.next());
//        }
//
//        return list;
//    }
}
