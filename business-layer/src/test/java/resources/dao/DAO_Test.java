package resources.dao;

import za.co.taylor.entity.BaseEntity;

import java.util.List;

public interface DAO_Test<T extends BaseEntity> {

    String create(T entity);
    T update(T entity);
    String delete(String id);
    List<T> getAll();
    T rowExists(String username, String password);
    T getById(String id);


//    void getOne(T entity);
//    String createCustomer(T entity);
//    int updateCustomer(T entity);
//    int deleteCustomer(String id);
//    List<T> getAllCustomers();

}
