package tests.services;

import resources.dao.DAO_Test;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import resources.services.DiscountService_Test;
import za.co.taylor.entity.DiscountEntity;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DiscountServiceTest {

    @Mock
    private DAO_Test daoInterfaceTest;

    @InjectMocks
    private DiscountService_Test serviceTest;

    @Test
    public void testCreate(){
        DiscountEntity de = new DiscountEntity();

        when(daoInterfaceTest.create(de)).thenReturn("test");

        Assert.assertEquals("test",serviceTest.addDiscount(de));

        verify(daoInterfaceTest, atLeastOnce()).create(de);
    }

    @Test
    public void testUpdate(){
        DiscountEntity de = new DiscountEntity();

        when(daoInterfaceTest.update(de)).thenReturn(de);

        Assert.assertEquals(de,serviceTest.updateDiscount(de));

        verify(daoInterfaceTest, atLeastOnce()).update(de);
    }

    @Test
    public void testDelete(){
        String id = "test";

        when(daoInterfaceTest.delete(id)).thenReturn(id);

        Assert.assertEquals(id, serviceTest.deleteDiscount(id));

        verify(daoInterfaceTest, atLeastOnce()).delete(id);
    }

    @Test
    public void testGet(){
        List<DiscountEntity> list = new ArrayList<>();

        when(daoInterfaceTest.getAll()).thenReturn(list);

        Assert.assertEquals(list,serviceTest.getDiscounts());

        verify(daoInterfaceTest, atLeastOnce()).getAll();
    }
}
