package tests.services;

import resources.dao.DAO_Test;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import resources.services.LoginService_Test;
import za.co.taylor.entity.CustomerEntity;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceTest {

    @Mock
    private DAO_Test daoInterfaceTest;

    @InjectMocks
    private LoginService_Test loginServiceTest;

    @Test
    public void testLogin(){
        String username = "teeray";
        String password = "1234";
        CustomerEntity customer = new CustomerEntity();

        when(daoInterfaceTest.rowExists(username, password)).thenReturn(customer);

        Assert.assertEquals(customer, loginServiceTest.login(username, password));

        verify(daoInterfaceTest, atLeastOnce()).rowExists(username, password);
    }
}
