package tests.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import resources.dao.DAO_Test;
import resources.services.RegistrationService_Test;
import za.co.taylor.entity.CustomerEntity;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationServiceTest {

    @Mock
    private DAO_Test<CustomerEntity> daoInterfaceTest;

    @InjectMocks
    private RegistrationService_Test registrationServiceTest;

    @Test
    public void testRegister(){
        CustomerEntity customer = new CustomerEntity();
        customer.set_id("123");

        when(daoInterfaceTest.create(customer)).thenReturn("123");

        Assert.assertEquals("123", registrationServiceTest.register(customer));

        verify(daoInterfaceTest, atLeastOnce()).create(customer);
    }
}
