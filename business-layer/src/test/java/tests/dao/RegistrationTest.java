package tests.dao;

import resources.dao.DAOImpl_Test;
import resources.dao.DAO_Test;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import za.co.taylor.entity.CustomerEntity;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationTest {

    private DAO_Test<CustomerEntity> dao = new DAOImpl_Test<>(CustomerEntity.class,"customertest");
    private CustomerEntity customer = new CustomerEntity();
    private CustomerEntity savedCustomer;
    private String id = "";

    @Before
    public void create(){
        customer.set_id("2222");
        customer.setName("Taylor");
        customer.setSurname("Ray");
        customer.setUsername("teeray");
        customer.setPassword("P@ssw0rd");
        customer.setCellphoneNumber("0825678495");
        customer.setEmailAddress("one@two.co.za");

        id = dao.create(customer);
        savedCustomer = dao.getById(id);

        Assert.assertNotNull(savedCustomer);
    }

    @Test
    public void update(){
        customer.set_id(id);
        customer.setName("Hey");
        customer.setSurname("man");
        customer.setUsername("yoyo123");
        customer.setPassword("P@sss");
        customer.setCellphoneNumber("43623462");
        customer.setEmailAddress("three@two.co.za");

        CustomerEntity updatedCustomer = dao.update(customer);

        Assert.assertEquals(savedCustomer.get_id(), updatedCustomer.get_id());
        Assert.assertNotEquals(savedCustomer.getName(), updatedCustomer.getName());
        Assert.assertNotEquals(savedCustomer.getSurname(), updatedCustomer.getSurname());
        Assert.assertNotEquals(savedCustomer.getUsername(), updatedCustomer.getUsername());
        Assert.assertNotEquals(savedCustomer.getPassword(), updatedCustomer.getPassword());
        Assert.assertNotEquals(savedCustomer.getCellphoneNumber(), updatedCustomer.getCellphoneNumber());
        Assert.assertNotEquals(savedCustomer.getEmailAddress(), updatedCustomer.getEmailAddress());
    }

    @Test
    public void getAll(){
        List<CustomerEntity> list = this.dao.getAll();

        Assert.assertEquals(1, list.size());
    }

    @After
    public void delete(){
        dao.delete(id);
        CustomerEntity deletedCustomer = dao.getById(id);

        Assert.assertNull(deletedCustomer);
    }
}
