package tests.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import resources.dao.DAO_Test;
import resources.dao.DAOImpl_Test;
import za.co.taylor.entity.DiscountEntity;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DiscountTest {

    private DAO_Test<DiscountEntity> dao = new DAOImpl_Test<>(DiscountEntity.class, "apitest");
    private DiscountEntity discount = new DiscountEntity();
    private DiscountEntity savedDiscount;
    private String id = "";

    @Before
    public void testCreate(){
        discount.set_id("1111");
        discount.setDescription("aaa");
        discount.setDiscountedPrice(8000);
        discount.setImages(new ArrayList<String>());
        discount.setOriginalPrice(9100);
        discount.setStoreId("2134");

        this.id = this.dao.create(discount);
        this.savedDiscount = dao.getById(this.id);

        Assert.assertNotNull(savedDiscount);
    }

    @Test
    public void testUpdate(){
        //update entity
        discount.set_id(this.id);
        discount.setDescription("bbb");
        discount.setDiscountedPrice(6000);
        discount.setOriginalPrice(7000);
        discount.setStoreId("2222");

        DiscountEntity updatedDiscount = dao.update(discount);

        Assert.assertEquals(savedDiscount.get_id(), updatedDiscount.get_id());
        Assert.assertNotEquals(savedDiscount.getDescription(), updatedDiscount.getDescription());
        Assert.assertNotEquals(savedDiscount.getDiscountedPrice(), updatedDiscount.getDiscountedPrice());
        Assert.assertNotEquals(savedDiscount.getOriginalPrice(), updatedDiscount.getOriginalPrice());
        Assert.assertNotEquals(savedDiscount.getStoreId(), updatedDiscount.getStoreId());
    }

    @Test
    public void testGet(){
        List<DiscountEntity> list;
        list = dao.getAll();
        Assert.assertEquals(1, list.size());
    }

    @After
    public void testDelete(){
        dao.delete(id);
        DiscountEntity checkDeleted = dao.getById(id);
        Assert.assertNull(checkDeleted);
    }
}
