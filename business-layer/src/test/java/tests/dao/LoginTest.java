package tests.dao;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import resources.dao.DAOImpl_Test;
import resources.dao.DAO_Test;
import za.co.taylor.entity.CustomerEntity;

public class LoginTest {

    private static DAO_Test<CustomerEntity> dao;
    private static final String username = "teeray";
    private static final String password = "1234";
    private static String id;

    @BeforeClass
    public static void setup() {
       dao  = new DAOImpl_Test<>(CustomerEntity.class, "customertest");CustomerEntity customer = new CustomerEntity();

       customer.setUsername(username);
       customer.setPassword(password);

       id = dao.create(customer);
       CustomerEntity savedCustomer = dao.getById(id);

       Assert.assertNotNull(savedCustomer);
    }

    @Test
    public void userExists(){
        CustomerEntity customer = dao.rowExists(username, password);
        Assert.assertNotNull(customer);
    }

    @AfterClass
    public static void teardown(){
        dao.delete(id);
        CustomerEntity checkDeleted = dao.getById(id);

        Assert.assertNull(checkDeleted);
    }
}