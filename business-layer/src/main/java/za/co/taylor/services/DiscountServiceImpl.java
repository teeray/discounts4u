package za.co.taylor.services;

import za.co.taylor.dao.DAO;
import za.co.taylor.dao.DAOImpl;
import za.co.taylor.entity.DiscountEntity;

import java.util.List;

public class DiscountServiceImpl {

    DAO<DiscountEntity> dao;

    public DiscountServiceImpl() {
        this.dao= new DAOImpl<>(DiscountEntity.class,"apitest");
    }

    public List<DiscountEntity> getDiscounts(){
        return dao.getAll();
    }

    public void deleteDiscount(String id){
        dao.delete(id);
    }

    public void addDiscount(DiscountEntity de){
        dao.create(de);
    }

    public void updateDiscount(DiscountEntity d){
        dao.update(d);
    }
}
