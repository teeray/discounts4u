package za.co.taylor.services;

import za.co.taylor.entity.CustomerEntity;

public interface RegistrationService {
    void register(CustomerEntity ce);
}
