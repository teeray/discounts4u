package za.co.taylor.services;

import za.co.taylor.entity.CustomerEntity;

public interface LoginService {
    CustomerEntity login(String username, String password);
}
