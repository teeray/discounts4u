package za.co.taylor.services;

import za.co.taylor.dao.DAO;
import za.co.taylor.dao.DAOImpl;
import za.co.taylor.entity.CustomerEntity;

public class LoginServiceImpl implements LoginService {

    DAO dao;

    @Override
    public CustomerEntity login(String username, String password) {
        dao = new DAOImpl<>(CustomerEntity.class, "customertest");
        CustomerEntity customer = (CustomerEntity)dao.rowExists(username, password);

        if ( customer != null){
            return customer;
        } else return null;
    }
}
