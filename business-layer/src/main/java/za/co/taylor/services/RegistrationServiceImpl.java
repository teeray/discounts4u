package za.co.taylor.services;

import za.co.taylor.dao.DAO;
import za.co.taylor.dao.DAOImpl;
import za.co.taylor.entity.CustomerEntity;

public class RegistrationServiceImpl implements RegistrationService {

    DAO<CustomerEntity> dao;

    public RegistrationServiceImpl(){
        this.dao = new DAOImpl<>(CustomerEntity.class,"customertest");
    }

    @Override
    public void register(CustomerEntity ce) {
        this.dao.create(ce);
    }
}
