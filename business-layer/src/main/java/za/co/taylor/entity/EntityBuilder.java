package za.co.taylor.entity;

import org.modelmapper.ModelMapper;
import za.co.taylor.dto.Customer;
import za.co.taylor.dto.Discount;

public class EntityBuilder {
    public DiscountEntity dtoToDiscountEntity(Discount dto){
        //ModelMapper mm = new ModelMapper();
        //DiscountEntity discountEntity = mm.map(dto,DiscountEntity.class);\
        DiscountEntity discountEntity = new DiscountEntity();

        discountEntity.setDescription(dto.getDescription());
        discountEntity.set_id(dto.getProductId());
        discountEntity.setDiscountedPrice(dto.getDiscountedPrice());
        discountEntity.setOriginalPrice(dto.getOriginalPrice());
        discountEntity.setStoreId(dto.getStoreId());
        discountEntity.setImages(dto.getImages());
        return discountEntity;
    }

    public CustomerEntity dtoToCustomerEntity(Customer dto){
        CustomerEntity ce = new CustomerEntity();

        ce.setName(dto.getName());
        ce.setSurname(dto.getSurname());
        ce.setUsername(dto.getUsername());
        ce.setPassword(dto.getPassword());
        ce.setCellphoneNumber(dto.getCellphoneNumber());
        ce.setEmailAddress(dto.getEmailAddress());

        return ce;
    }
}
