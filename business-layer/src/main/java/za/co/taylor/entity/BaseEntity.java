package za.co.taylor.entity;

import org.mongojack.Id;

import java.util.Date;
import java.util.UUID;

public class BaseEntity {
    @Id
    private String _id;
    private Date dateCreated;
    private Date lastUpdate;

    public void initData(){
        if (this._id == null) {
            this._id = (UUID.randomUUID().toString());
        }
        this.dateCreated = new Date(System.currentTimeMillis());
        this.lastUpdate = new Date(System.currentTimeMillis());
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
