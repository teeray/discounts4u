package za.co.taylor.entity;

import java.util.ArrayList;
import java.util.List;

public class DiscountEntity extends BaseEntity{

    private String storeId = null;
    private Integer originalPrice = null;
    private Integer discountedPrice = null;
    private List<String> images = new ArrayList<>();
    private String description = null;

    public DiscountEntity() {
    }

    public DiscountEntity(String storeId, Integer originalPrice, Integer discountedPrice, List<String> images, String description) {
        this.storeId = storeId;
        this.originalPrice = originalPrice;
        this.discountedPrice = discountedPrice;
        this.images = images;
        this.description = description;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public void setDiscountedPrice(Integer discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStoreId() {
        return storeId;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }

    public Integer getDiscountedPrice() {
        return discountedPrice;
    }

    public List<String> getImages() {
        return images;
    }

    public String getDescription() {
        return description;
    }

}
