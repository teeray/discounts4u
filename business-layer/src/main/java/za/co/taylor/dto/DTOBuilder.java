package za.co.taylor.dto;

import za.co.taylor.entity.CustomerEntity;
import za.co.taylor.entity.DiscountEntity;

public interface DTOBuilder {
    Discount entityToDiscountDTO(DiscountEntity de);
    Customer entityToCustomerDTO(CustomerEntity ce);
}
