package za.co.taylor.dto;

import za.co.taylor.entity.CustomerEntity;
import za.co.taylor.entity.DiscountEntity;

public class DTOBuilderImpl implements DTOBuilder{

    public Discount entityToDiscountDTO(DiscountEntity de){
        return new Discount()
                .productId(de.get_id())
                .storeId(de.getStoreId())
                .originalPrice(de.getOriginalPrice())
                .discountedPrice(de.getDiscountedPrice())
                .images(de.getImages())
                .description(de.getDescription());
    }

    public Customer entityToCustomerDTO(CustomerEntity ce){
        return new Customer()
                .name(ce.getName())
                .surname(ce.getSurname())
                .username(ce.getUsername())
                .password(ce.getPassword())
                .cellphoneNumber(ce.getCellphoneNumber())
                .emailAddress(ce.getEmailAddress());
    }
}
