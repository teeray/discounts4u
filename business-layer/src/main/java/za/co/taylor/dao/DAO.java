package za.co.taylor.dao;

import za.co.taylor.entity.BaseEntity;
import za.co.taylor.entity.CustomerEntity;

import java.util.List;

public interface DAO<T extends BaseEntity> {
    void create(T entity);
    void update(T entity);
    void delete(String id);
    List<T> getAll();
    void getOne(T entity);
    T rowExists(String username, String password);
}
