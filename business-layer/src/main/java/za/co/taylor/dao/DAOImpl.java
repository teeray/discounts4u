package za.co.taylor.dao;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import org.mongojack.DBCursor;
import org.mongojack.DBQuery;
import org.mongojack.JacksonDBCollection;
import za.co.taylor.entity.BaseEntity;
import za.co.taylor.entity.CustomerEntity;
import za.co.taylor.entity.DiscountEntity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DAOImpl<T extends BaseEntity> implements DAO<T>{
    private JacksonDBCollection<T,String> wrappedCollection;

    public DAOImpl(){

    }

    public DAOImpl(Class<T> tClass, String collName){
        Mongo client = new Mongo("localhost", 27017);
        DB db = client.getDB("mongotest");
        DBCollection coll = db.getCollection(collName);
        wrappedCollection = JacksonDBCollection.wrap(coll, tClass, String.class);
    }

    @Override
    public void create(T entity) {
        if (!rowExists(entity.get_id())) {
            entity.initData();
            this.wrappedCollection.insert(entity);
        }
    }

    @Override
    public void update(T entity) {
        T oldEntity = wrappedCollection.findOneById(entity.get_id());

        entity.setDateCreated(oldEntity.getDateCreated());
        entity.setLastUpdate(new Date(System.currentTimeMillis()));
        this.wrappedCollection.save(entity);
    }

    @Override
    public void delete(String id) {
        //DBQuery.Query query = DBQuery.is("_id", id);
        this.wrappedCollection.removeById(id);
    }

    @Override
    public List<T> getAll() {
        List<T> list = new ArrayList<>();
        DBCursor<T> cursor = wrappedCollection.find();

        while (cursor.hasNext()){
            list.add(cursor.next());
        }
        return list;
    }

    @Override
    public void getOne(T entity) {
        System.out.println(wrappedCollection.findOneById(entity.get_id()));
    }

    @Override
    public T rowExists(String username, String password) {
        DBQuery.Query checkUsername = DBQuery.is("username", username);

        DBCursor<T> cursor = this.wrappedCollection.find(checkUsername);

        if(cursor.size() == 0){
            return null;
        } else {
            CustomerEntity customerToCheck = (CustomerEntity) cursor.next();

            if (customerToCheck.getPassword().equals(password)){
                return (T)customerToCheck;
            } else return null;
        }
    }

    public boolean rowExists(String id){
        T entity = this.wrappedCollection.findOneById(id);

        return entity == null ? false : true;
    }
}
