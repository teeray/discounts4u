package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * Discount
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-08-15T07:34:59.018Z")

public class Discount   {
  @JsonProperty("productId")
  private String productId = null;

  @JsonProperty("storeId")
  private Integer storeId = null;

  @JsonProperty("originalPrice")
  private Integer originalPrice = null;

  @JsonProperty("discountedPrice")
  private Integer discountedPrice = null;

  @JsonProperty("images")
  private List<String> images = new ArrayList<String>();

  @JsonProperty("description")
  private String description = null;

  public Discount productId(String productId) {
    this.productId = productId;
    return this;
  }

   /**
   * product id
   * @return productId
  **/
  @ApiModelProperty(example = "512", required = true, value = "product id")
  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Discount storeId(Integer storeId) {
    this.storeId = storeId;
    return this;
  }

   /**
   * store id
   * @return storeId
  **/
  @ApiModelProperty(example = "4160", required = true, value = "store id")
  public Integer getStoreId() {
    return storeId;
  }

  public void setStoreId(Integer storeId) {
    this.storeId = storeId;
  }

  public Discount originalPrice(Integer originalPrice) {
    this.originalPrice = originalPrice;
    return this;
  }

   /**
   * original price of the item
   * @return originalPrice
  **/
  @ApiModelProperty(example = "100", required = true, value = "original price of the item")
  public Integer getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(Integer originalPrice) {
    this.originalPrice = originalPrice;
  }

  public Discount discountedPrice(Integer discountedPrice) {
    this.discountedPrice = discountedPrice;
    return this;
  }

   /**
   * discounted price of the item
   * @return discountedPrice
  **/
  @ApiModelProperty(example = "80", required = true, value = "discounted price of the item")
  public Integer getDiscountedPrice() {
    return discountedPrice;
  }

  public void setDiscountedPrice(Integer discountedPrice) {
    this.discountedPrice = discountedPrice;
  }

  public Discount images(List<String> images) {
    this.images = images;
    return this;
  }

  public Discount addImagesItem(String imagesItem) {
    this.images.add(imagesItem);
    return this;
  }

   /**
   * Get images
   * @return images
  **/
  @ApiModelProperty(required = true, value = "")
  public List<String> getImages() {
    return images;
  }

  public void setImages(List<String> images) {
    this.images = images;
  }

  public Discount description(String description) {
    this.description = description;
    return this;
  }

   /**
   * the description of the discount
   * @return description
  **/
  @ApiModelProperty(example = "Clover milk on sale now!", required = true, value = "the description of the discount")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Discount discount = (Discount) o;
    return Objects.equals(this.productId, discount.productId) &&
        Objects.equals(this.storeId, discount.storeId) &&
        Objects.equals(this.originalPrice, discount.originalPrice) &&
        Objects.equals(this.discountedPrice, discount.discountedPrice) &&
        Objects.equals(this.images, discount.images) &&
        Objects.equals(this.description, discount.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(productId, storeId, originalPrice, discountedPrice, images, description);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Discount {\n");
    
    sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
    sb.append("    storeId: ").append(toIndentedString(storeId)).append("\n");
    sb.append("    originalPrice: ").append(toIndentedString(originalPrice)).append("\n");
    sb.append("    discountedPrice: ").append(toIndentedString(discountedPrice)).append("\n");
    sb.append("    images: ").append(toIndentedString(images)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

