package io.swagger.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.taylor.dto.Discount;

import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-08-15T07:34:59.018Z")

@Api(value = "discounts", description = "the discounts API")
public interface DiscountsApi {

    @ApiOperation(value = "", notes = "adds a discount to the db", response = Void.class, tags={ "discounts", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "discount added", response = Void.class),
        @ApiResponse(code = 500, message = "error occurred", response = Void.class) })
    @RequestMapping(value = "/discounts",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> addDiscount(@ApiParam(value = "the discount to add"  ) @RequestBody Discount discount);


    @ApiOperation(value = "", notes = "deletes a discount from the db", response = Void.class, tags={ "discounts", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Void.class) })
    @RequestMapping(value = "/discounts/{productId}",
        produces = { "application/json" },
        method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteDiscount(@ApiParam(value = "the code to see which discount to delete",required=true ) @PathVariable("productId") String productId);


    @ApiOperation(value = "", notes = "gets all discounts from the db", response = Void.class, tags={ "discounts", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Void.class) })
    @RequestMapping(value = "/discounts",
        produces = { "application/json" },
        method = RequestMethod.GET)
    public ResponseEntity<List<Discount>> getDiscounts();


    @ApiOperation(value = "", notes = "updates a discount in the db", response = Void.class, tags={ "discounts", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "discount updated", response = Void.class) })
    @RequestMapping(value = "/discounts/{productId}",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.PUT)
    ResponseEntity<Void> updateDiscount(@ApiParam(value = "the updated discount" ,required=true ) @RequestBody Discount discount);

}
