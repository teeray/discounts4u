package io.swagger.api;

import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import za.co.taylor.dto.DTOBuilder;
import za.co.taylor.dto.DTOBuilderImpl;
import za.co.taylor.dto.Discount;
import za.co.taylor.entity.DiscountEntity;
import za.co.taylor.entity.EntityBuilder;
import za.co.taylor.services.DiscountServiceImpl;

import java.util.ArrayList;
import java.util.List;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-08-15T07:34:59.018Z")

@Controller
public class DiscountsApiController implements DiscountsApi {

    @Autowired
    DiscountServiceImpl discountService;

    public ResponseEntity<Void> addDiscount(@ApiParam(value = "the discount to add"  ) @RequestBody Discount discount) {
        DiscountEntity entity = new EntityBuilder().dtoToDiscountEntity(discount);
        this.discountService.addDiscount(entity);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> deleteDiscount(@ApiParam(value = "the code to see which discount to delete",required=true ) @PathVariable("productId") String productId) {
        this.discountService.deleteDiscount(productId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<List<Discount>> getDiscounts() {
        List<DiscountEntity> entityList = this.discountService.getDiscounts();

        DTOBuilder dtoBuilder = new DTOBuilderImpl();
        List<Discount> dtoList = new ArrayList<>();

        for(DiscountEntity de : entityList){
            Discount dto = dtoBuilder.entityToDiscountDTO(de);
            dtoList.add(dto);
        }

        return new ResponseEntity<List<Discount>>(dtoList,HttpStatus.OK);
    }

    public ResponseEntity<Void> updateDiscount(@ApiParam(value = "the updated discount" ,required=true ) @RequestBody Discount discount) {

        DiscountEntity entity = new EntityBuilder().dtoToDiscountEntity(discount);
        this.discountService.updateDiscount(entity);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
